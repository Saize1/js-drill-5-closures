function counterFactory(){
    let countervariable=0;//lexical environment

    function increment(){
        countervariable+=1;
        return countervariable
    }
    function decrement(){
        countervariable-=1
        return countervariable;
    }
     return {increment,decrement};
} 
module.exports=counterFactory;