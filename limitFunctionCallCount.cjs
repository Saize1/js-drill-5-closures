    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

function limitFunctionCall(cb,n){
    if(!cb || !n){
        throw new Error("No parameter passed")
    }
   let count = 0; 
    function invokecb(...args){
        if(count<n){
            count++;
            return cb(...args)
        }
        else{
           return null;
        }
    }
    return invokecb;
}
module.exports=limitFunctionCall;

